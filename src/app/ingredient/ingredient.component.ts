import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IngredientType } from 'src/entity/IngredientType';
import { Ingredient } from 'src/entity/Ingredient';
import { IngredientService } from '../service/ingredient.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {

  ingredient: Ingredient;
  
  ingredientform: FormGroup;

  constructor(private ingredientService: IngredientService) { }

  ngOnInit() {

    this.ingredientform = new FormGroup({
      name: new FormControl('', Validators.required),
    });

  }

  onSubmit() {    
    this.ingredientService.save(this.ingredientform.get('name').value).subscribe(test => console.log(test));
  }

}
