import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ingredient } from 'src/entity/Ingredient';
import { Observable } from 'rxjs';

@Injectable()
export class IngredientService {

  private ingredientUrl: string;

  constructor(private http: HttpClient) {
    this.ingredientUrl = 'http://localhost:8080/saveIngredient';
   }

   public save(ingredient: string){
     return this.http.post(this.ingredientUrl, ingredient);
   }

   public findAll(): Observable<IngredientService[]> {
     return this.http.get<IngredientService[]>(this.ingredientUrl);
   }
}
