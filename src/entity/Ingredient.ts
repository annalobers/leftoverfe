import { IngredientType } from './IngredientType'

export class Ingredient {

    name: string;
    ingredientType: IngredientType;
}