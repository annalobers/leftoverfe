export enum IngredientType{
    MEAT = 'Meat',
    FISH = 'Fish',
    ANIMAL_PRODUCT = 'Animal products',
    FRUIT = 'Fruit',
    VEGETABLE = 'Vegetable',
    SPICE = 'Spice',
    GRAIN = 'Grain',
    LEGUME= 'Legume'
}
